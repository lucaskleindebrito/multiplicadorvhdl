library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Multiplicador is
	port(
		clk	: in std_logic;
		en_7s	: out std_logic_vector(3 downto 0);
		disp	: out	std_logic_vector(7 downto 0);
		
		clkd_p : in std_logic;  
		clkd_n : in std_logic;  
		bit0_p : in std_logic;  
		bit0_n : in std_logic;

		dado0_out : out std_logic_vector(3 downto 0);
		dado1_out : out std_logic_vector(3 downto 0);
		dado2_out : out std_logic_vector(3 downto 0);
		dado3_out : out std_logic_vector(3 downto 0);
		clk_in_multi	   : out std_logic;
		sinal_input_multi : out std_logic;
		clk_enable : out std_logic;
		contador3 : out std_logic_vector(3 downto 0)  
	);
end Multiplicador;

architecture Multiplicador of Multiplicador is
	
	signal clk_in	    : std_logic;
	signal sinal_input : std_logic;
	
	signal clk_1k	: std_logic;
	signal contador : std_logic_vector(1 downto 0):= "00";
	
	signal to_disp	:	std_logic_vector(3 downto 0);
	
	signal fator_1 : std_logic_vector(7 downto 0);
	signal fator_2 : std_logic_vector(7 downto 0);
	signal produto	: std_logic_vector(15 downto 0);
	
	signal dado0	:	std_logic_vector(3 downto 0);
	signal dado1	:	std_logic_vector(3 downto 0);
	signal dado2	:	std_logic_vector(3 downto 0);
	signal dado3	:	std_logic_vector(3 downto 0);
	
	signal dezena1 : std_logic_vector(15 downto 0);
	signal dezena11 : std_logic_vector(7 downto 0);

	signal dezena2 : std_logic_vector(15 downto 0);
	signal dezena22 : std_logic_vector(7 downto 0);
	
	signal conv_unidade : std_logic_vector(3 downto 0);
	signal conv_dezena : std_logic_vector(3 downto 0);
	signal conv_centena : std_logic_vector(3 downto 0);
	signal conv_milhar : std_logic_vector(3 downto 0);
	
	constant DEZ : std_logic_vector (7 downto 0) := "00001010";
	

	signal dado22:std_logic_vector(7 downto 0);
	signal dado00:std_logic_vector(7 downto 0);
	
	signal dado11:std_logic_vector(7 downto 0);
	signal dado33:std_logic_vector(7 downto 0);
	
begin
	dado0_out <= dado0;
	dado1_out <= dado1;
	dado2_out <= dado2;
	dado3_out <= dado3;
	clk_in_multi <= clk_in;
	sinal_input_multi <= sinal_input;
	
	
	dado22 (3 downto 0) <= dado2;
	dado22 (7 downto 4) <= (others => '0');
	
	dado11 (3 downto 0) <= dado1;
	dado11 (7 downto 4) <= (others => '0');
	
	dado33 (3 downto 0) <= dado3;
	dado33 (7 downto 4) <= (others => '0');
	
	dado00 (3 downto 0) <= dado0;
	dado00 (7 downto 4) <= (others => '0');
	
	dezena11 (7 downto 0) <= dezena1(7 downto 0);
	dezena22 (7 downto 0) <= dezena2(7 downto 0);	
	
	fator_1 <= dezena11 + dado33;
	fator_2 <= dezena22 + dado11;
	
	--produto <= fator_1 * fator_2;
	
	disp_mod: entity work.bcd_7s
	port map(
		point	=> '1',
  		bcd_in => to_disp,
  		sete_segmentos	=> disp
		
	);
	
	clk_1k_mod:	entity work.clk_1k
	port map(
		clk => clk,
		clk_1k=> clk_1k
	);
	
	sep_mod: entity work.separar
	port map(
		clk => clk,
		produto => produto,
		unidade => conv_unidade, 
		dezena => conv_dezena,
		centena => conv_centena,
		milhar => conv_milhar
	);

	
	mult_mod: entity work.Multiplicador_Assincrono
		generic map(
			DATA_WIDTH=>8
		)
		port map(
			numero1=>dado22,
			numero2=>DEZ,
			p=>dezena1
		);
		
	mult_mod2: entity work.Multiplicador_Assincrono
		generic map(
			DATA_WIDTH=>8
		)
		port map(
			numero1=>dado00,
			numero2=>DEZ,
			p=>dezena2
		);
		
	mult_mod3: entity work.Multiplicador_Assincrono
		generic map(
			DATA_WIDTH=>8
		)
		port map(
			numero1=>fator_1,
			numero2=>fator_2,
			p=>produto
		);
		
	display_process: process(clk_1k)
	begin
                if clk_1k'event and clk_1k = '1' then

                case contador is
                    when "00" =>
                        en_7s <= "1110";
                        to_disp <= conv_milhar;
                        
                    when "01" =>
                        en_7s <= "1101";
                        to_disp <= conv_centena;

                    when "10" =>
                        en_7s <= "1011";
                        to_disp <= conv_dezena;

                    when others =>
                        en_7s <= "0111";
                        to_disp <= conv_unidade;

                end case;

                    contador <= contador + "01";

                    if contador = "11" then
						  
                        contador <= "00";
								
                    end if;
                end if;
	end process display_process;
		
	input_dados: entity work.input_buffer
	port map(
		clk_i  => clk_in,
		clkd_p => clkd_p,
		clkd_n => clkd_n,  
		bit0   => sinal_input, 
		bit0_p => bit0_p,
		bit0_n => bit0_n
	);
		
	
 fsm_receptor: entity work.fsm_receptor
 port map(
		clk            => clk,
		valorDisplay0  => dado1,
		valorDisplay1  => dado0,
		valorDisplay2  => dado3,
	   valorDisplay3  => dado2,
		bitEntrada     => sinal_input,
		clk_in         => clk_in,
		clk_enable2     => clk_enable,
		contador2 => contador3
	);
	
end Multiplicador;


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY tb_multiplicador IS
END tb_multiplicador;
 
ARCHITECTURE behavior OF tb_multiplicador IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT Multiplicador
    PORT(
         clk : IN  std_logic;
         dado0 : IN  std_logic_vector(3 downto 0);
         dado1 : IN  std_logic_vector(3 downto 0);
         dado2 : IN  std_logic_vector(3 downto 0);
         dado3 : IN  std_logic_vector(3 downto 0);
         en_7s : OUT  std_logic_vector(3 downto 0);
         disp : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;

   --Inputs
   signal clk : std_logic := '0';
   signal dado0 : std_logic_vector(3 downto 0) := (others => '0');
   signal dado1 : std_logic_vector(3 downto 0) := (others => '0');
   signal dado2 : std_logic_vector(3 downto 0) := (others => '0');
   signal dado3 : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal en_7s : std_logic_vector(3 downto 0);
   signal disp : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Multiplicador PORT MAP (
          clk => clk,
          dado0 => dado0,
          dado1 => dado1,
          dado2 => dado2,
          dado3 => dado3,
          en_7s => en_7s,
          disp => disp
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;

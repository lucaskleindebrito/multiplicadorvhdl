
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name Multiplicador -dir "H:/Brito/Nova pasta/Multiplicador/Multiplicador/planAhead_run_1" -part xc3s700afg484-4
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "H:/Brito/Nova pasta/Multiplicador/Multiplicador/Multiplicador.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {H:/Brito/Nova pasta/Multiplicador/Multiplicador} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "Multiplicador.ucf" [current_fileset -constrset]
add_files [list {Multiplicador.ucf}] -fileset [get_property constrset [current_run]]
link_design

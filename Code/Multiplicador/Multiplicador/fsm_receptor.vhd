library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity fsm_receptor is
    Port ( clk           : in  STD_LOGIC;
			  valorDisplay0 : inout std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 0
			  valorDisplay1 : inout std_logic_vector(3 downto 0) := "0000";	 -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 1
		     valorDisplay2 : inout std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 2
	        valorDisplay3 : inout std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 3
			  bitEntrada    : in STD_LOGIC;
			  clk_in        : in STD_LOGIC;		 
			  clk_enable2	 : out STD_LOGIC;
			  contador2		 : out std_logic_vector(3 downto 0)  
	 );
end fsm_receptor;

architecture fsm_receptor of fsm_receptor is
	constant UM : std_logic_vector(3 downto 0) := "0001";
	type maquina_estados is (esperar, receber);	
	signal estado 	     : maquina_estados;	
	signal clk_enable   : STD_LOGIC;
	signal contador     : std_logic_vector(3 downto 0) := "1111";
begin
	contador2 <= contador;
	
	-- Inst�ncia do m�dulo "single_shot" para o single_en do clock 500khz --
	single_shot_clk_500k: entity work.single_shot
	port map
		( 
			clk 		=> clk,
			sgn_in 	=> clk_in,
			sgn_out 	=> clk_enable
		);
	
	fsm_process:process (clk)
		begin
			if clk'event and clk='1' then				
					case estado is													
						when esperar => 										
							if bitEntrada = '1' and clk_enable = '1' then 
								estado <= esperar;
							elsif clk_enable = '1' then
								estado <= receber;
							end if;
						when receber =>
							if contador = "1111" and clk_enable = '1' then
								valorDisplay3(3) <= bitEntrada;						
							elsif contador = "1110" and clk_enable = '1' then
								valorDisplay3(2) <= bitEntrada;	
							elsif contador = "1101" and clk_enable = '1' then 
								valorDisplay3(1) <= bitEntrada;
							elsif contador = "1100" and clk_enable = '1' then 
								valorDisplay3(0) <= bitEntrada;
							elsif contador = "1011" and clk_enable = '1' then 
								valorDisplay2(3) <= bitEntrada;
							elsif contador = "1010" and clk_enable = '1' then 
								valorDisplay2(2) <= bitEntrada;
							elsif contador = "1001" and clk_enable = '1'  then 
								valorDisplay2(1) <= bitEntrada;
							elsif contador = "1000" and clk_enable = '1' then 
								valorDisplay2(0) <= bitEntrada;
							elsif contador = "0111" and clk_enable = '1' then 
								valorDisplay1(3) <= bitEntrada;
							elsif contador = "0110" and clk_enable = '1' then 
								valorDisplay1(2) <= bitEntrada;
							elsif contador = "0101" and clk_enable = '1' then 
								valorDisplay1(1) <= bitEntrada;
							elsif contador = "0100" and clk_enable = '1' then 
								valorDisplay1(0) <= bitEntrada;
							elsif contador = "0011" and clk_enable = '1' then 
								valorDisplay0(3) <= bitEntrada;
							elsif contador = "0010" and clk_enable = '1' then 
								valorDisplay0(2) <= bitEntrada;
							elsif contador = "0001" and clk_enable = '1' then 
								valorDisplay0(1) <= bitEntrada;
							elsif contador = "0000" and clk_enable = '1' then 
								valorDisplay0(0) <= bitEntrada;			
								estado <= esperar;
								contador <= "1111";
							end if;
					end case;	
					
					if estado = receber and clk_enable = '1' then
						contador <= contador - UM;
					end if;
			
			end if;
		end process fsm_process;

end fsm_receptor;
library ieee;
use ieee.std_logic_1164.all;
library unisim;
use unisim.vcomponents.all;

entity mult is
	generic (
		NUM_BITS	:	integer := 18
	);
	port (
		rst	:	in		std_logic;
		clk	:	in		std_logic;
		a		:	in		std_logic_vector(NUM_BITS - 1 downto 0);
		b		:	in		std_logic_vector(NUM_BITS - 1 downto 0);
		x		:	out	std_logic_vector((NUM_BITS * 2) - 1 downto 0)
	);
end mult;

architecture mult of mult is
	
	signal a_int	:	std_logic_vector(17 downto 0) := (others => '0');
	signal b_int	:	std_logic_vector(17 downto 0) := (others => '0');
	signal x_int	:	std_logic_vector(35 downto 0) := (others => '0');

begin

	x		<= x_int((NUM_BITS * 2) - 1 downto 0);
	a_int(NUM_BITS - 1 downto 0)	<= a;
	b_int(NUM_BITS - 1 downto 0)	<= b;
   
   MULT18X18S_inst : MULT18X18S
   port map (
      P 	=> x_int,
      A 	=> a_int,
      B 	=> b_int,
      C 	=> clk,
      CE => '1',
      R 	=> rst
   );

end mult;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity separar is
	port(
		clk	:	in std_logic;
		produto	:	in	std_logic_vector(15 downto 0);
		unidade	:	out	std_logic_vector(3 downto 0);
		dezena	:	out	std_logic_vector(3 downto 0);
		centena	:	out	std_logic_vector(3 downto 0);
		milhar	:	out	std_logic_vector(3 downto 0)
	);
end separar;

architecture separar of separar is


	constant ZERO : std_logic_vector (3 downto 0) := "0000";
	constant UM : std_logic_vector (3 downto 0) := "0001";
	constant DOIS : std_logic_vector (3 downto 0) := "0010";
	constant TRES : std_logic_vector (3 downto 0) := "0011";
	constant QUATRO : std_logic_vector (3 downto 0) := "0100";
	constant CINCO : std_logic_vector (3 downto 0) := "0101";
	constant SEIS : std_logic_vector (3 downto 0) := "0110";
	constant SETE : std_logic_vector (3 downto 0) := "0111";
	constant OITO : std_logic_vector (3 downto 0) := "1000";
	constant NOVE : std_logic_vector (3 downto 0) := "1001";


	constant ZERO16 : std_logic_vector (15 downto 0) := "0000000000000000";
	constant UM16 : std_logic_vector (15 downto 0) := "0000000000000001";
	constant DOIS16 : std_logic_vector (15 downto 0) := "0000000000000010";
	constant TRES16 : std_logic_vector (15 downto 0) := "0000000000000011";
	constant QUATRO16 : std_logic_vector (15 downto 0) := "0000000000000100";
	constant CINCO16 : std_logic_vector (15 downto 0) := "0000000000000101";
	constant SEIS16 : std_logic_vector (15 downto 0) := "0000000000000110";
	constant SETE16 : std_logic_vector (15 downto 0) := "0000000000000111";
	constant OITO16 : std_logic_vector (15 downto 0) := "0000000000001000";
	constant NOVE16 : std_logic_vector (15 downto 0) := "0000000000001001";
	--------------------------------------------------------------------
	
	constant DEZ : std_logic_vector (15 downto 0) := "0000000000001010";
	constant VINTE : std_logic_vector (15 downto 0) := "0000000000010100";
	constant TRINTA : std_logic_vector (15 downto 0) := "0000000000011110";
	constant QUARENTA : std_logic_vector (15 downto 0) := "0000000000101000";
	constant CINQUENTA : std_logic_vector (15 downto 0) := "0000000000110010";
	constant SESSENTA : std_logic_vector (15 downto 0) := "0000000000111100";
	constant SETENTA : std_logic_vector (15 downto 0) := "0000000001000110";
	constant OITENTA : std_logic_vector (15 downto 0) := "0000000001010000";
	constant NOVENTA : std_logic_vector (15 downto 0) := "0000000001011010";
	
	--------------------------------------------------------------------
	constant CEM : std_logic_vector (15 downto 0) := "0000000001100100";
	constant DUZENTOS : std_logic_vector (15 downto 0) := "0000000011001000";
	constant TREZENTOS : std_logic_vector (15 downto 0) := "0000000100101100";
	constant QUATROCENTOS : std_logic_vector (15 downto 0) := "0000000110010000";
	constant QUINHENTOS : std_logic_vector (15 downto 0) := "0000000111110100";
	constant SEISCENTOS : std_logic_vector (15 downto 0) := "0000001001011000";
	constant SETECENTOS : std_logic_vector (15 downto 0) := "0000001010111100";
	constant OITOCENTOS : std_logic_vector (15 downto 0) := "0000001100100000";
	constant NOVECENTOS : std_logic_vector (15 downto 0) := "0000001110000100";
	-------------------------------------------------------------------
	
	constant MIL : std_logic_vector (15 downto 0) := "0000001111101000";
	constant DOISMIL : std_logic_vector (15 downto 0) := "0000011111010000";
	constant TRESMIL : std_logic_vector (15 downto 0) := "0000101110111000";
	constant QUATROMIL : std_logic_vector (15 downto 0) := "0000111110100000";
	constant CINCOMIL : std_logic_vector (15 downto 0) := "0001001110001000";
	constant SEISMIL : std_logic_vector (15 downto 0) := "0001011101110000";
	constant SETEMIL : std_logic_vector (15 downto 0) := "0001101101011000";
	constant OITOMIL : std_logic_vector (15 downto 0) := "0001111101000000";
	constant NOVEMIL : std_logic_vector (15 downto 0) := "0010001100101000";
	
	signal p : std_logic_vector(15 downto 0);
	signal p1 : std_logic_vector(15 downto 0);
	signal p2 : std_logic_vector(15 downto 0);
	signal p3 : std_logic_vector(15 downto 0);
	
	--signal cnt : std_logic_vector(15 downto 0) := "0010001100101000";
	
begin
	
	--rela��o:
	--milhar <= p;
	--centena <= p1;
	--dezena <= p2;
	--unidade <= p3;

	separar_process: process(clk)
	begin
		
		p <= produto;
		
		if clk'event and clk = '1' then
		
			if p >= NOVEMIL then
				p1 <= p - NOVEMIL;
				milhar <= NOVE;
			elsif p >= OITOMIL then
				p1 <= p - OITOMIL;
				milhar <= OITO;
			elsif p >= SETEMIL then
				p1 <= p - SETEMIL;
				milhar <= SETE;
			elsif p >= SEISMIL then
				p1 <= p - SEISMIL;
				milhar <= SEIS;
			elsif p >= CINCOMIL then
				p1 <= p - CINCOMIL;
				milhar <= CINCO;
			elsif p >= QUATROMIL then
				p1 <= p - QUATROMIL;
				milhar <= QUATRO;
			elsif p >= TRESMIL then
				p1 <= p - TRESMIL;
				milhar <= TRES;
			elsif p >= DOISMIL then
				p1 <= p - DOISMIL;
				milhar <= DOIS;
			elsif p >= MIL then
				p1 <= p - MIL;
				milhar <= UM;
			else
				p1 <= p;
				milhar <= ZERO;
			end if;
			
			-------------------
			
			if p1 >= NOVECENTOS then
				p2 <= p1 - NOVECENTOS;
				centena <= NOVE;
			elsif p1 >= OITOCENTOS then
				p2 <= p1 - OITOCENTOS;
				centena <= OITO;
			elsif p1 >= SETECENTOS then
				p2 <= p1 - SETECENTOS;
				centena <= SETE;
			elsif p1 >= SEISCENTOS then
				p2 <= p1 - SEISCENTOS;
				centena <= SEIS;
			elsif p1 >= QUINHENTOS then
				p2 <= p1 - QUINHENTOS;
				centena <= CINCO;
			elsif p1 >= QUATROCENTOS then
				p2 <= p1 - QUATROCENTOS;
				centena <= QUATRO;
			elsif p1 >= TREZENTOS then
				p2 <= p1 - TREZENTOS;
				centena <= TRES;
			elsif p1 >= DUZENTOS then
				p2 <= p1 - DUZENTOS;
				centena <= DOIS;
			elsif p1 >= CEM then
				p2 <= p1 - CEM;
				centena <= UM;
			else
				p2 <= p1;
				centena <= ZERO;
			end if;
		
			-------------------
			
			if p2 >= NOVENTA then
				p3 <= p2 - NOVENTA;
				dezena <= NOVE;
			elsif p2 >= OITENTA then
				p3 <= p2 - OITENTA;
				dezena <= OITO;
			elsif p2 >= SETENTA then
				p3 <= p2 - SETENTA;
				dezena <= SETE;
			elsif p2 >= SESSENTA then
				p3 <= p2 - SESSENTA;
				dezena <= SEIS;
			elsif p2 >= CINQUENTA then
				p3 <= p2 - CINQUENTA;
				dezena <= CINCO;
			elsif p2 >= QUARENTA then
				p3 <= p2 - QUARENTA;
				dezena <= QUATRO;
			elsif p2 >= TRINTA then
				p3 <= p2 - TRINTA;
				dezena <= TRES;
			elsif p2 >= VINTE then
				p3 <= p2 - VINTE;
				dezena <= DOIS;
			elsif p2 >= DEZ then
				p3 <= p2 - DEZ;
				dezena <= UM;
			else
				p3 <= p2;
				dezena <= ZERO;
			end if;
			
			---------------------------------
			
			if p3 >= NOVE16 then
				unidade <= NOVE;
			elsif p3 >= OITO16 then
				unidade <= OITO;
			elsif p3 >= SETE16 then
				unidade <= SETE;
			elsif p3 >= SEIS16 then
				unidade <= SEIS;
			elsif p3 >= CINCO16 then
				unidade <= CINCO;
			elsif p3 >= QUATRO16 then
				unidade <= QUATRO;
			elsif p3 >= TRES16 then
				unidade <= TRES;
			elsif p3 >= DOIS16 then
				unidade <= DOIS;
			elsif p3 >= UM16 then
				unidade <= UM;
			else
				unidade <= ZERO;
			end if;
			
		end if;		

	
		

	end process separar_process;

end separar;


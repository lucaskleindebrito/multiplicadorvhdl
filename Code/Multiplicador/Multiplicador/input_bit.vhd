library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity input_bit is
	Port ( 
		clk          : in  STD_LOGIC;
		valorDisplay : out std_logic_vector(3 downto 0) := "0000";
		inputBit     : in STD_LOGIC;
		avancar      : out STD_LOGIC
	 );
end input_bit;

architecture input_bit of input_bit is
	type maquina_estados is (rx_bit1, rx_bit2, rx_bit3, rx_bit4);
	signal estado 	 : maquina_estados;                 
begin
	fsm_process:process (clk)
	begin
		if clk'event and clk='1' then
			case estado is			
				when rx_bit1 => 
					avancar <= '0';
					valorDisplay(0) <= inputBit;
					estado <= rx_bit2;						
				when rx_bit2 => 
					valorDisplay(1) <= inputBit;
					estado <= rx_bit3;						
				when rx_bit3 => 
					valorDisplay(2) <= inputBit;
					estado <= rx_bit4;
				when rx_bit4 => 
					valorDisplay(3) <= inputBit;
					estado <= rx_bit1;
					avancar <= '1';
			end case;
		end if;
	end process fsm_process;
end input_bit;



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity clk_1k is
	port (
		clk	:	in std_logic;
		clk_1k	: out std_logic
	);

end clk_1k;

architecture clk_1k of clk_1k is
	signal contador	:	std_logic_vector	(8 downto 0);
	


begin
	
	process(clk)
	begin
		
		if clk'event and clk='1' then
			contador <= contador + "000000001";
			if contador = "111111111" then
				clk_1k <= '1';
				contador <= "000000000";
			else
				clk_1k <='0';
			end if;
		end if;
	
	
	end process;
end clk_1k;


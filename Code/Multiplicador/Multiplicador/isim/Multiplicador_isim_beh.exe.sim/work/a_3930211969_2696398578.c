/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Brito/Documents/Repositorio/MultiplicadorVHDL/Code/Multiplicador/Multiplicador/fsm_receptor.vhd";
extern char *IEEE_P_3620187407;

char *ieee_p_3620187407_sub_767668596_3965413181(char *, char *, char *, char *, char *, char *);


static void work_a_3930211969_2696398578_p_0(char *t0)
{
    char t18[16];
    char t23[16];
    unsigned char t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    unsigned char t6;
    unsigned char t7;
    char *t8;
    unsigned char t9;
    char *t10;
    char *t11;
    unsigned char t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    unsigned int t24;
    static char *nl0[] = {&&LAB9, &&LAB10, &&LAB11, &&LAB12, &&LAB13};

LAB0:    xsi_set_current_line(35, ng0);
    t2 = (t0 + 992U);
    t3 = xsi_signal_has_event(t2);
    if (t3 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:
LAB3:    t2 = (t0 + 3952);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(36, ng0);
    t4 = (t0 + 2152U);
    t8 = *((char **)t4);
    t9 = *((unsigned char *)t8);
    t4 = (char *)((nl0) + t9);
    goto **((char **)t4);

LAB5:    t4 = (t0 + 1032U);
    t5 = *((char **)t4);
    t6 = *((unsigned char *)t5);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB7;

LAB8:    goto LAB3;

LAB9:    xsi_set_current_line(38, ng0);
    t10 = (t0 + 2472U);
    t11 = *((char **)t10);
    t12 = *((unsigned char *)t11);
    t13 = (t12 == (unsigned char)3);
    if (t13 != 0)
        goto LAB14;

LAB16:
LAB15:    goto LAB8;

LAB10:    xsi_set_current_line(42, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7768);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB20;

LAB21:    t1 = (unsigned char)0;

LAB22:    if (t1 != 0)
        goto LAB17;

LAB19:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7774);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB27;

LAB28:    t1 = (unsigned char)0;

LAB29:    if (t1 != 0)
        goto LAB25;

LAB26:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7780);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB34;

LAB35:    t1 = (unsigned char)0;

LAB36:    if (t1 != 0)
        goto LAB32;

LAB33:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7786);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB41;

LAB42:    t1 = (unsigned char)0;

LAB43:    if (t1 != 0)
        goto LAB39;

LAB40:
LAB18:    goto LAB8;

LAB11:    xsi_set_current_line(57, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7792);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB47;

LAB48:    t1 = (unsigned char)0;

LAB49:    if (t1 != 0)
        goto LAB44;

LAB46:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7798);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB54;

LAB55:    t1 = (unsigned char)0;

LAB56:    if (t1 != 0)
        goto LAB52;

LAB53:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7804);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB61;

LAB62:    t1 = (unsigned char)0;

LAB63:    if (t1 != 0)
        goto LAB59;

LAB60:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7810);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB68;

LAB69:    t1 = (unsigned char)0;

LAB70:    if (t1 != 0)
        goto LAB66;

LAB67:
LAB45:    goto LAB8;

LAB12:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7816);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB74;

LAB75:    t1 = (unsigned char)0;

LAB76:    if (t1 != 0)
        goto LAB71;

LAB73:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7822);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB81;

LAB82:    t1 = (unsigned char)0;

LAB83:    if (t1 != 0)
        goto LAB79;

LAB80:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7828);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB88;

LAB89:    t1 = (unsigned char)0;

LAB90:    if (t1 != 0)
        goto LAB86;

LAB87:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7834);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB95;

LAB96:    t1 = (unsigned char)0;

LAB97:    if (t1 != 0)
        goto LAB93;

LAB94:
LAB72:    goto LAB8;

LAB13:    xsi_set_current_line(87, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7840);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB101;

LAB102:    t1 = (unsigned char)0;

LAB103:    if (t1 != 0)
        goto LAB98;

LAB100:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7846);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB108;

LAB109:    t1 = (unsigned char)0;

LAB110:    if (t1 != 0)
        goto LAB106;

LAB107:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7852);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB115;

LAB116:    t1 = (unsigned char)0;

LAB117:    if (t1 != 0)
        goto LAB113;

LAB114:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7858);
    t10 = (t18 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t5, t18);
    if (t3 == 1)
        goto LAB122;

LAB123:    t1 = (unsigned char)0;

LAB124:    if (t1 != 0)
        goto LAB120;

LAB121:
LAB99:    goto LAB8;

LAB14:    xsi_set_current_line(39, ng0);
    t10 = (t0 + 4032);
    t14 = (t10 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = (unsigned char)1;
    xsi_driver_first_trans_fast(t10);
    goto LAB15;

LAB17:    xsi_set_current_line(43, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4096);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 3U, 1, 0LL);
    xsi_set_current_line(44, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7771);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB23;

LAB24:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB18;

LAB20:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB22;

LAB23:    xsi_size_not_matching(3U, t24, 0);
    goto LAB24;

LAB25:    xsi_set_current_line(46, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4096);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 2U, 1, 0LL);
    xsi_set_current_line(47, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7777);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB30;

LAB31:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB18;

LAB27:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB29;

LAB30:    xsi_size_not_matching(3U, t24, 0);
    goto LAB31;

LAB32:    xsi_set_current_line(49, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4096);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 1U, 1, 0LL);
    xsi_set_current_line(50, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7783);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB37;

LAB38:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB18;

LAB34:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB36;

LAB37:    xsi_size_not_matching(3U, t24, 0);
    goto LAB38;

LAB39:    xsi_set_current_line(52, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4096);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 0U, 1, 0LL);
    xsi_set_current_line(53, ng0);
    t2 = (t0 + 4032);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((unsigned char *)t10) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(54, ng0);
    t2 = (t0 + 7789);
    t5 = (t0 + 4160);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    t11 = (t10 + 56U);
    t14 = *((char **)t11);
    memcpy(t14, t2, 3U);
    xsi_driver_first_trans_fast(t5);
    goto LAB18;

LAB41:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB43;

LAB44:    xsi_set_current_line(58, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4224);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 3U, 1, 0LL);
    xsi_set_current_line(59, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7795);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB50;

LAB51:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB45;

LAB47:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB49;

LAB50:    xsi_size_not_matching(3U, t24, 0);
    goto LAB51;

LAB52:    xsi_set_current_line(61, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4224);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 2U, 1, 0LL);
    xsi_set_current_line(62, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7801);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB57;

LAB58:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB45;

LAB54:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB56;

LAB57:    xsi_size_not_matching(3U, t24, 0);
    goto LAB58;

LAB59:    xsi_set_current_line(64, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4224);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 1U, 1, 0LL);
    xsi_set_current_line(65, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7807);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB64;

LAB65:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB45;

LAB61:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB63;

LAB64:    xsi_size_not_matching(3U, t24, 0);
    goto LAB65;

LAB66:    xsi_set_current_line(67, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4224);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 0U, 1, 0LL);
    xsi_set_current_line(68, ng0);
    t2 = (t0 + 4032);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((unsigned char *)t10) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(69, ng0);
    t2 = (t0 + 7813);
    t5 = (t0 + 4160);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    t11 = (t10 + 56U);
    t14 = *((char **)t11);
    memcpy(t14, t2, 3U);
    xsi_driver_first_trans_fast(t5);
    goto LAB45;

LAB68:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB70;

LAB71:    xsi_set_current_line(73, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4288);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 3U, 1, 0LL);
    xsi_set_current_line(74, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7819);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB77;

LAB78:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB72;

LAB74:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB76;

LAB77:    xsi_size_not_matching(3U, t24, 0);
    goto LAB78;

LAB79:    xsi_set_current_line(76, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4288);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 2U, 1, 0LL);
    xsi_set_current_line(77, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7825);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB84;

LAB85:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB72;

LAB81:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB83;

LAB84:    xsi_size_not_matching(3U, t24, 0);
    goto LAB85;

LAB86:    xsi_set_current_line(79, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4288);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 1U, 1, 0LL);
    xsi_set_current_line(80, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7831);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB91;

LAB92:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB72;

LAB88:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB90;

LAB91:    xsi_size_not_matching(3U, t24, 0);
    goto LAB92;

LAB93:    xsi_set_current_line(82, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4288);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 0U, 1, 0LL);
    xsi_set_current_line(83, ng0);
    t2 = (t0 + 4032);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((unsigned char *)t10) = (unsigned char)4;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(84, ng0);
    t2 = (t0 + 7837);
    t5 = (t0 + 4160);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    t11 = (t10 + 56U);
    t14 = *((char **)t11);
    memcpy(t14, t2, 3U);
    xsi_driver_first_trans_fast(t5);
    goto LAB72;

LAB95:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB97;

LAB98:    xsi_set_current_line(88, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4352);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 3U, 1, 0LL);
    xsi_set_current_line(89, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7843);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB104;

LAB105:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB99;

LAB101:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB103;

LAB104:    xsi_size_not_matching(3U, t24, 0);
    goto LAB105;

LAB106:    xsi_set_current_line(91, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4352);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 2U, 1, 0LL);
    xsi_set_current_line(92, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7849);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB111;

LAB112:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB99;

LAB108:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB110;

LAB111:    xsi_size_not_matching(3U, t24, 0);
    goto LAB112;

LAB113:    xsi_set_current_line(94, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4352);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 1U, 1, 0LL);
    xsi_set_current_line(95, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 7720U);
    t5 = (t0 + 7855);
    t10 = (t23 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 0;
    t11 = (t10 + 4U);
    *((int *)t11) = 2;
    t11 = (t10 + 8U);
    *((int *)t11) = 1;
    t19 = (2 - 0);
    t20 = (t19 * 1);
    t20 = (t20 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t20;
    t11 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t5, t23);
    t14 = (t18 + 12U);
    t20 = *((unsigned int *)t14);
    t24 = (1U * t20);
    t1 = (3U != t24);
    if (t1 == 1)
        goto LAB118;

LAB119:    t15 = (t0 + 4160);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 3U);
    xsi_driver_first_trans_fast(t15);
    goto LAB99;

LAB115:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB117;

LAB118:    xsi_size_not_matching(3U, t24, 0);
    goto LAB119;

LAB120:    xsi_set_current_line(97, ng0);
    t11 = (t0 + 1832U);
    t15 = *((char **)t11);
    t9 = *((unsigned char *)t15);
    t11 = (t0 + 4352);
    t16 = (t11 + 56U);
    t17 = *((char **)t16);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = t9;
    xsi_driver_first_trans_delta(t11, 0U, 1, 0LL);
    xsi_set_current_line(98, ng0);
    t2 = (t0 + 4032);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((unsigned char *)t10) = (unsigned char)0;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(99, ng0);
    t2 = (t0 + 7861);
    t5 = (t0 + 4160);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    t11 = (t10 + 56U);
    t14 = *((char **)t11);
    memcpy(t14, t2, 3U);
    xsi_driver_first_trans_fast(t5);
    goto LAB99;

LAB122:    t11 = (t0 + 2472U);
    t14 = *((char **)t11);
    t6 = *((unsigned char *)t14);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB124;

}


extern void work_a_3930211969_2696398578_init()
{
	static char *pe[] = {(void *)work_a_3930211969_2696398578_p_0};
	xsi_register_didat("work_a_3930211969_2696398578", "isim/Multiplicador_isim_beh.exe.sim/work/a_3930211969_2696398578.didat");
	xsi_register_executes(pe);
}

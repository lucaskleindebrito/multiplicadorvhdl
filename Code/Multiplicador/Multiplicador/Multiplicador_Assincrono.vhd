
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Multiplicador_Assincrono is
	generic(
			DATA_WIDTH :	integer
	);
	
	port(
		numero1 : in std_logic_vector(DATA_WIDTH-1 DOWNTO 0);
		numero2 : in std_logic_vector(DATA_WIDTH-1 DOWNTO 0);
		p       : out std_logic_vector(2*DATA_WIDTH -1 DOWNTO 0)
		);
end Multiplicador_Assincrono;

architecture Multiplicador_Assincrono of Multiplicador_Assincrono is
	signal num1_int : std_logic_vector(17 downto 0);
	signal num2_int : std_logic_vector(17 downto 0);
	signal p_int : std_logic_vector(35 downto 0);
	
begin

	num1_int(DATA_WIDTH-1 downto 0) <=numero1;
	num1_int(17 downto DATA_WIDTH) <=(others =>'0');
	
	num2_int(DATA_WIDTH-1 downto 0) <=numero2;
	num2_int(17 downto DATA_WIDTH) <=(others =>'0');
	
	p <= p_int((2*DATA_WIDTH)-1 downto 0); 
	
	-----------------------------------------------
	
	MULT18X18_inst : MULT18X18
   port map (
      P => p_int,    -- 36-bit multiplier output
      A => num1_int,    -- 18-bit multiplier input
      B => num2_int     -- 18-bit multiplier input
   );
	
end Multiplicador_Assincrono;



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity input_buffer is
	port(
		clk_i  : out std_logic; 
		clkd_p : in std_logic;  
		clkd_n : in std_logic;  
		
		bit0    : out std_logic; 
		bit0_p : in std_logic;  
		bit0_n : in std_logic		
	);
end input_buffer;

architecture input_buffer of input_buffer is

begin

	-- Instancia do bloco inst_clk
	inst_clk : IBUFGDS
   generic map (
      DIFF_TERM => FALSE,      -- Differential Termination (Spartan-3E/5 only)
      IBUF_DELAY_VALUE => "0", -- Especificar a quantidade de atraso de entrada adicionada para buffer, "0"-"12" 
      IOSTANDARD => "DEFAULT")
   port map (
      O => clk_i,   -- Buffer output
      I => clkd_p,  -- Diff_p buffer entrada 
      IB => clkd_n  -- Diff_n buffer entrada
   );
   
	-- Instancia do bloco inbuf_inst_1
	inbuf_inst_1 : IBUFDS
   generic map (
      DIFF_TERM => FALSE,        
      IBUF_DELAY_VALUE => "0",   
      IFD_DELAY_VALUE => "AUTO", -- Especifique a quantidade de atraso adicionado para registo de entrada, "AUTO", "0"-"6" 
      IOSTANDARD => "DEFAULT")
   port map (
      O => bit0,     -- Buffer output
      I => bit0_p,  -- Diff_p buffer input 
      IB => bit0_n  -- Diff_n buffer input 
   );
end input_buffer;


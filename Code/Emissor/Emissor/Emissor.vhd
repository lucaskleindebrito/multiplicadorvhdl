-- Bibliotecas --
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Entidade --
entity Emissor is
	Port ( clk              : in  STD_LOGIC;                      -- Clock do pino B8.
			 botao_incremento : in  STD_LOGIC;                      -- Bot�o de incremento.
          botao_decremento : in  STD_LOGIC;                      -- Bot�o de decremento.
			 switch_operando1 : in  STD_LOGIC;                      -- Switch para incrementar ou decrementar o operando 1.
			 switch_operando2 : in  STD_LOGIC;                      -- Switch para incrementar ou decrementar o operando 2.
			 display7seg      : out	std_logic_vector(7 downto 0);	  -- Sa�da para o Display de 7 segmentos.
          enableDisplay    : out  STD_LOGIC_VECTOR (3 downto 0); -- S�ida para habilitar o display de 7 segmentos que deve ficar visivel
			
			 -- Portas para o envio das informa��es 			 
			 botao_enviar  : in STD_LOGIC;			 
			 swd_p : out std_logic;
			 swd_n : out std_logic;			 
			 clk_p  : out std_logic;
			 clk_n  : out std_logic;
			------------------------------------------------------------	
			 valorDisplay0_out : out std_logic_vector(3 downto 0);    --
			 valorDisplay1_out : out std_logic_vector(3 downto 0);	  -- 
			 valorDisplay2_out : out std_logic_vector(3 downto 0);    --
			 valorDisplay3_out : out std_logic_vector(3 downto 0);--
			 bitSaida_out : out std_logic;--
			 clk_500k_out : out std_logic--
			);	
end Emissor;

-- Arquitetura --
architecture Emissor of Emissor is
	-- Sinais de controle --
	signal valor : std_logic_vector(3 downto 0);                      -- Sinal responsavel por informar o valor que deve ser exibido no display habilitado.
	signal clk_1k : STD_LOGIC;														-- Sinal responsavel por determinar a frequencia de exibi�ao do valor nos display's, permitindo assim a visualiza��o humana.
	signal clk_500k : STD_LOGIC := '0';											-- Sinal responsavel por determinar a frequencia de exibi�ao do valor nos display's, permitindo assim a visualiza��o humana.
	signal valorDisplay0 : std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 0
	signal valorDisplay1 : std_logic_vector(3 downto 0) := "0000";	   -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 1
	signal valorDisplay2 : std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 2
	signal valorDisplay3 : std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 3
	signal btn_incrementa : STD_LOGIC;											-- Sinal responsavel por determinar o click no bot�o responsavel por incrementar o valor do operando selecionado
	signal btn_decrementa : STD_LOGIC;											-- Sinal responsavel por determinar o click no bot�o responsavel por decrementar o valor do operando selecionado
	signal btn_enviar     : STD_LOGIC;											-- Sinal responsavel por determinar o click no bot�o responsavel por enviar o valor dos operandos para a placa de multiplica��o

	signal bitSaida : STD_LOGIC :='1';   				                        -- Sinal responsavel por informar o valor do bit a ser enviado no periodo
begin
	valorDisplay0_out <= valorDisplay0;
	valorDisplay1_out <= valorDisplay1;
	valorDisplay2_out <= valorDisplay2;
	valorDisplay3_out <= valorDisplay3;	
	bitSaida_out <= bitSaida;
	clk_500k_out <= clk_500k;
	-- Inst�ncia do m�dulo "single_shot" para o bot�o de incremento --
	single_shot_incremento: entity work.single_shot
		port map
			( 
				clk 		=> clk,
				sgn_in 	=> botao_incremento,
				sgn_out 	=> btn_incrementa
			);
	
	-- Inst�ncia do m�dulo "single_shot" para o bot�o que decrementa --
	single_shot_decremento: entity work.single_shot
		port map
			( 
				clk 		=> clk,
				sgn_in 	=> botao_decremento,
				sgn_out 	=> btn_decrementa
			);
			
	-- Inst�ncia do m�dulo "single_shot" para o bot�o que envia as informa��es para serem multiplicadas --
	single_shot_enviar: entity work.single_shot
		port map
			( 
				clk 		=> clk_500k,
				sgn_in 	=> botao_enviar,
				sgn_out 	=> btn_enviar
			);
	
	-- Inst�ncia do m�dulo "bcd_7s" para imprimir valores nos display's de 7 segmentos --
	view_display: entity work.bcd_7s
		port map( 
				point 	=> '1',
	        	bcd_in 	=> valor,
	        	sete_segmentos => display7seg
		);
	
	
	-- Inst�ncia do m�dulo "gerador_clk_1k" para permitir a visualiza��o humana de forma nitida das informa��es nos display's --
	clock_1k: entity work.gerador_clk_1k
		port map( 
				clk 	 => clk,
	        	clk_1k => clk_1k
		);	
		
	-- Inst�ncia do m�dulo "gerador_clk_500k" para permitir a comunica��o com o multiplicador --
	clock_500k: entity work.gerador_clk_500k
		port map( 
				clk 	 => clk,
	        	clk_500k => clk_500k
		);	
	
	-- Inst�ncia do m�dulo "fsm_display" responsavel por selecionar o valor que deve aparecer no display habilitado seguindo a velocidade do clock de 1k --
	view_selector: entity work.fsm_display
		port map( 
	        	clk_1k => clk_1k,
				valorDisplay0 => valorDisplay0,
				valorDisplay1 => valorDisplay1,
				valorDisplay2 => valorDisplay2,
				valorDisplay3 => valorDisplay3,
				enableDisplay => enableDisplay,
				valor         => valor
		);			
		
	-- Inst�ncia do m�dulo "fsm_display" responsavel por controlar o incremento e decremento dos operandos --
	processa_operandos: entity work.processa_operando
		port map( 
	        	clk => clk,
				btn_incrementa => btn_incrementa,
				btn_decrementa => btn_decrementa,
				switch_operando1 => switch_operando1,
				switch_operando2 => switch_operando2,				
				valorDisplay0 => valorDisplay0,
				valorDisplay1 => valorDisplay1,
				valorDisplay2 => valorDisplay2,
				valorDisplay3 => valorDisplay3
		);	
		
	-- Inst�ncia do m�dulo "out_buffer" responsavel por controlar o envio das informa��es para a segunda placa --
	out_buffer: entity work.out_buffer
		port  map(
			bit1   => bitSaida,
			clk_o  => clk_500k, 
			swd_p  => swd_p,
			swd_n  => swd_n,
			clk_p  => clk_p,
			clk_n  => clk_n
		);
		
	-- Inst�ncia do m�dulo "fsm_envio" responsavel por controlar os valores que devem ser enviados para a segunda placa e quando devem ser enviados --
	fsm_emissor: entity work.fsm_envio
		port map( 
				clk_500k      => clk_500k,
				btn_enviar    => btn_enviar,
				valorDisplay0 => valorDisplay0,
				valorDisplay1 => valorDisplay1,
				valorDisplay2 => valorDisplay2,				
				valorDisplay3 => valorDisplay3,				
				bitSaida      => bitSaida
		);		
		
		
end Emissor;


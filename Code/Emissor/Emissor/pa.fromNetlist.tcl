
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name Emissor -dir "C:/Users/Brito/Documents/Repositorio/MultiplicadorVHDL/Code/Emissor/Emissor/planAhead_run_5" -part xc3s100ecp132-5
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/Brito/Documents/Repositorio/MultiplicadorVHDL/Code/Emissor/Emissor/Emissor.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/Brito/Documents/Repositorio/MultiplicadorVHDL/Code/Emissor/Emissor} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "Emissor.ucf" [current_fileset -constrset]
add_files [list {Emissor.ucf}] -fileset [get_property constrset [current_run]]
link_design

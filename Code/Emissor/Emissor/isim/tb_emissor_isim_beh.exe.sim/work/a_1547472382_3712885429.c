/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Brito/Documents/Repositorio/MultiplicadorVHDL/Code/Emissor/Emissor/processa_operando.vhd";
extern char *IEEE_P_3620187407;

char *ieee_p_3620187407_sub_767668596_3965413181(char *, char *, char *, char *, char *, char *);
char *ieee_p_3620187407_sub_767740470_3965413181(char *, char *, char *, char *, char *, char *);


static void work_a_1547472382_3712885429_p_0(char *t0)
{
    char t18[16];
    unsigned char t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    unsigned char t6;
    unsigned char t7;
    char *t8;
    unsigned char t9;
    unsigned char t10;
    unsigned char t11;
    char *t12;
    unsigned char t13;
    unsigned char t14;
    char *t15;
    unsigned char t16;
    unsigned char t17;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned char t26;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;

LAB0:    xsi_set_current_line(37, ng0);
    t2 = (t0 + 992U);
    t3 = xsi_signal_has_event(t2);
    if (t3 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:
LAB3:    t2 = (t0 + 4992);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(38, ng0);
    t4 = (t0 + 1192U);
    t8 = *((char **)t4);
    t9 = *((unsigned char *)t8);
    t10 = (t9 == (unsigned char)3);
    if (t10 != 0)
        goto LAB8;

LAB10:    t2 = (t0 + 1672U);
    t4 = *((char **)t2);
    t1 = *((unsigned char *)t4);
    t3 = (t1 == (unsigned char)3);
    if (t3 != 0)
        goto LAB42;

LAB43:
LAB9:    goto LAB3;

LAB5:    t4 = (t0 + 1032U);
    t5 = *((char **)t4);
    t6 = *((unsigned char *)t5);
    t7 = (t6 == (unsigned char)3);
    t1 = t7;
    goto LAB7;

LAB8:    xsi_set_current_line(39, ng0);
    t4 = (t0 + 1352U);
    t12 = *((char **)t4);
    t13 = *((unsigned char *)t12);
    t14 = (t13 == (unsigned char)3);
    if (t14 == 1)
        goto LAB14;

LAB15:    t11 = (unsigned char)0;

LAB16:    if (t11 != 0)
        goto LAB11;

LAB13:    t2 = (t0 + 1512U);
    t4 = *((char **)t2);
    t3 = *((unsigned char *)t4);
    t6 = (t3 == (unsigned char)3);
    if (t6 == 1)
        goto LAB29;

LAB30:    t1 = (unsigned char)0;

LAB31:    if (t1 != 0)
        goto LAB27;

LAB28:
LAB12:    goto LAB9;

LAB11:    xsi_set_current_line(40, ng0);
    t4 = (t0 + 1832U);
    t19 = *((char **)t4);
    t4 = (t0 + 9016U);
    t20 = (t0 + 2728U);
    t21 = *((char **)t20);
    t20 = (t0 + 9096U);
    t22 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t19, t4, t21, t20);
    t23 = (t18 + 12U);
    t24 = *((unsigned int *)t23);
    t25 = (1U * t24);
    t26 = (4U != t25);
    if (t26 == 1)
        goto LAB17;

LAB18:    t27 = (t0 + 5072);
    t28 = (t27 + 56U);
    t29 = *((char **)t28);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    memcpy(t31, t22, 4U);
    xsi_driver_first_trans_fast_port(t27);
    xsi_set_current_line(41, ng0);
    t2 = (t0 + 1832U);
    t4 = *((char **)t2);
    t2 = (t0 + 9016U);
    t5 = (t0 + 3688U);
    t8 = *((char **)t5);
    t5 = (t0 + 9224U);
    t1 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t8, t5);
    if (t1 != 0)
        goto LAB19;

LAB21:
LAB20:    goto LAB12;

LAB14:    t4 = (t0 + 1512U);
    t15 = *((char **)t4);
    t16 = *((unsigned char *)t15);
    t17 = (t16 == (unsigned char)2);
    t11 = t17;
    goto LAB16;

LAB17:    xsi_size_not_matching(4U, t25, 0);
    goto LAB18;

LAB19:    xsi_set_current_line(42, ng0);
    t12 = (t0 + 2608U);
    t15 = *((char **)t12);
    t12 = (t0 + 5072);
    t19 = (t12 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t15, 4U);
    xsi_driver_first_trans_fast_port(t12);
    xsi_set_current_line(43, ng0);
    t2 = (t0 + 1992U);
    t4 = *((char **)t2);
    t2 = (t0 + 9032U);
    t5 = (t0 + 2728U);
    t8 = *((char **)t5);
    t5 = (t0 + 9096U);
    t12 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t8, t5);
    t15 = (t18 + 12U);
    t24 = *((unsigned int *)t15);
    t25 = (1U * t24);
    t1 = (4U != t25);
    if (t1 == 1)
        goto LAB22;

LAB23:    t19 = (t0 + 5136);
    t20 = (t19 + 56U);
    t21 = *((char **)t20);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    memcpy(t23, t12, 4U);
    xsi_driver_first_trans_fast_port(t19);
    xsi_set_current_line(44, ng0);
    t2 = (t0 + 1992U);
    t4 = *((char **)t2);
    t2 = (t0 + 9032U);
    t5 = (t0 + 3688U);
    t8 = *((char **)t5);
    t5 = (t0 + 9224U);
    t1 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t8, t5);
    if (t1 != 0)
        goto LAB24;

LAB26:
LAB25:    goto LAB20;

LAB22:    xsi_size_not_matching(4U, t25, 0);
    goto LAB23;

LAB24:    xsi_set_current_line(45, ng0);
    t12 = (t0 + 2608U);
    t15 = *((char **)t12);
    t12 = (t0 + 5136);
    t19 = (t12 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t15, 4U);
    xsi_driver_first_trans_fast_port(t12);
    goto LAB25;

LAB27:    xsi_set_current_line(49, ng0);
    t2 = (t0 + 2152U);
    t8 = *((char **)t2);
    t2 = (t0 + 9048U);
    t12 = (t0 + 2728U);
    t15 = *((char **)t12);
    t12 = (t0 + 9096U);
    t19 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t8, t2, t15, t12);
    t20 = (t18 + 12U);
    t24 = *((unsigned int *)t20);
    t25 = (1U * t24);
    t10 = (4U != t25);
    if (t10 == 1)
        goto LAB32;

LAB33:    t21 = (t0 + 5200);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    t27 = (t23 + 56U);
    t28 = *((char **)t27);
    memcpy(t28, t19, 4U);
    xsi_driver_first_trans_fast_port(t21);
    xsi_set_current_line(50, ng0);
    t2 = (t0 + 2152U);
    t4 = *((char **)t2);
    t2 = (t0 + 9048U);
    t5 = (t0 + 3688U);
    t8 = *((char **)t5);
    t5 = (t0 + 9224U);
    t1 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t8, t5);
    if (t1 != 0)
        goto LAB34;

LAB36:
LAB35:    goto LAB12;

LAB29:    t2 = (t0 + 1352U);
    t5 = *((char **)t2);
    t7 = *((unsigned char *)t5);
    t9 = (t7 == (unsigned char)2);
    t1 = t9;
    goto LAB31;

LAB32:    xsi_size_not_matching(4U, t25, 0);
    goto LAB33;

LAB34:    xsi_set_current_line(51, ng0);
    t12 = (t0 + 2608U);
    t15 = *((char **)t12);
    t12 = (t0 + 5200);
    t19 = (t12 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t15, 4U);
    xsi_driver_first_trans_fast_port(t12);
    xsi_set_current_line(52, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 9064U);
    t5 = (t0 + 2728U);
    t8 = *((char **)t5);
    t5 = (t0 + 9096U);
    t12 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t18, t4, t2, t8, t5);
    t15 = (t18 + 12U);
    t24 = *((unsigned int *)t15);
    t25 = (1U * t24);
    t1 = (4U != t25);
    if (t1 == 1)
        goto LAB37;

LAB38:    t19 = (t0 + 5264);
    t20 = (t19 + 56U);
    t21 = *((char **)t20);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    memcpy(t23, t12, 4U);
    xsi_driver_first_trans_fast_port(t19);
    xsi_set_current_line(53, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 9064U);
    t5 = (t0 + 3688U);
    t8 = *((char **)t5);
    t5 = (t0 + 9224U);
    t1 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t8, t5);
    if (t1 != 0)
        goto LAB39;

LAB41:
LAB40:    goto LAB35;

LAB37:    xsi_size_not_matching(4U, t25, 0);
    goto LAB38;

LAB39:    xsi_set_current_line(54, ng0);
    t12 = (t0 + 2608U);
    t15 = *((char **)t12);
    t12 = (t0 + 5264);
    t19 = (t12 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t15, 4U);
    xsi_driver_first_trans_fast_port(t12);
    goto LAB40;

LAB42:    xsi_set_current_line(59, ng0);
    t2 = (t0 + 1352U);
    t5 = *((char **)t2);
    t7 = *((unsigned char *)t5);
    t9 = (t7 == (unsigned char)3);
    if (t9 == 1)
        goto LAB47;

LAB48:    t6 = (unsigned char)0;

LAB49:    if (t6 != 0)
        goto LAB44;

LAB46:    t2 = (t0 + 1512U);
    t4 = *((char **)t2);
    t3 = *((unsigned char *)t4);
    t6 = (t3 == (unsigned char)3);
    if (t6 == 1)
        goto LAB62;

LAB63:    t1 = (unsigned char)0;

LAB64:    if (t1 != 0)
        goto LAB60;

LAB61:
LAB45:    goto LAB9;

LAB44:    xsi_set_current_line(60, ng0);
    t2 = (t0 + 1832U);
    t12 = *((char **)t2);
    t2 = (t0 + 9016U);
    t15 = (t0 + 2728U);
    t19 = *((char **)t15);
    t15 = (t0 + 9096U);
    t20 = ieee_p_3620187407_sub_767740470_3965413181(IEEE_P_3620187407, t18, t12, t2, t19, t15);
    t21 = (t18 + 12U);
    t24 = *((unsigned int *)t21);
    t25 = (1U * t24);
    t13 = (4U != t25);
    if (t13 == 1)
        goto LAB50;

LAB51:    t22 = (t0 + 5072);
    t23 = (t22 + 56U);
    t27 = *((char **)t23);
    t28 = (t27 + 56U);
    t29 = *((char **)t28);
    memcpy(t29, t20, 4U);
    xsi_driver_first_trans_fast_port(t22);
    xsi_set_current_line(61, ng0);
    t2 = (t0 + 1832U);
    t4 = *((char **)t2);
    t2 = (t0 + 9016U);
    t5 = (t0 + 2608U);
    t8 = *((char **)t5);
    t5 = (t0 + 9080U);
    t1 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t8, t5);
    if (t1 != 0)
        goto LAB52;

LAB54:
LAB53:    goto LAB45;

LAB47:    t2 = (t0 + 1512U);
    t8 = *((char **)t2);
    t10 = *((unsigned char *)t8);
    t11 = (t10 == (unsigned char)2);
    t6 = t11;
    goto LAB49;

LAB50:    xsi_size_not_matching(4U, t25, 0);
    goto LAB51;

LAB52:    xsi_set_current_line(62, ng0);
    t12 = (t0 + 3688U);
    t15 = *((char **)t12);
    t12 = (t0 + 5072);
    t19 = (t12 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t15, 4U);
    xsi_driver_first_trans_fast_port(t12);
    xsi_set_current_line(63, ng0);
    t2 = (t0 + 1992U);
    t4 = *((char **)t2);
    t2 = (t0 + 9032U);
    t5 = (t0 + 2728U);
    t8 = *((char **)t5);
    t5 = (t0 + 9096U);
    t12 = ieee_p_3620187407_sub_767740470_3965413181(IEEE_P_3620187407, t18, t4, t2, t8, t5);
    t15 = (t18 + 12U);
    t24 = *((unsigned int *)t15);
    t25 = (1U * t24);
    t1 = (4U != t25);
    if (t1 == 1)
        goto LAB55;

LAB56:    t19 = (t0 + 5136);
    t20 = (t19 + 56U);
    t21 = *((char **)t20);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    memcpy(t23, t12, 4U);
    xsi_driver_first_trans_fast_port(t19);
    xsi_set_current_line(64, ng0);
    t2 = (t0 + 1992U);
    t4 = *((char **)t2);
    t2 = (t0 + 9032U);
    t5 = (t0 + 2608U);
    t8 = *((char **)t5);
    t5 = (t0 + 9080U);
    t1 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t8, t5);
    if (t1 != 0)
        goto LAB57;

LAB59:
LAB58:    goto LAB53;

LAB55:    xsi_size_not_matching(4U, t25, 0);
    goto LAB56;

LAB57:    xsi_set_current_line(65, ng0);
    t12 = (t0 + 3688U);
    t15 = *((char **)t12);
    t12 = (t0 + 5136);
    t19 = (t12 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t15, 4U);
    xsi_driver_first_trans_fast_port(t12);
    goto LAB58;

LAB60:    xsi_set_current_line(69, ng0);
    t2 = (t0 + 2152U);
    t8 = *((char **)t2);
    t2 = (t0 + 9048U);
    t12 = (t0 + 2728U);
    t15 = *((char **)t12);
    t12 = (t0 + 9096U);
    t19 = ieee_p_3620187407_sub_767740470_3965413181(IEEE_P_3620187407, t18, t8, t2, t15, t12);
    t20 = (t18 + 12U);
    t24 = *((unsigned int *)t20);
    t25 = (1U * t24);
    t10 = (4U != t25);
    if (t10 == 1)
        goto LAB65;

LAB66:    t21 = (t0 + 5200);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    t27 = (t23 + 56U);
    t28 = *((char **)t27);
    memcpy(t28, t19, 4U);
    xsi_driver_first_trans_fast_port(t21);
    xsi_set_current_line(70, ng0);
    t2 = (t0 + 2152U);
    t4 = *((char **)t2);
    t2 = (t0 + 9048U);
    t5 = (t0 + 2608U);
    t8 = *((char **)t5);
    t5 = (t0 + 9080U);
    t1 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t8, t5);
    if (t1 != 0)
        goto LAB67;

LAB69:
LAB68:    goto LAB45;

LAB62:    t2 = (t0 + 1352U);
    t5 = *((char **)t2);
    t7 = *((unsigned char *)t5);
    t9 = (t7 == (unsigned char)2);
    t1 = t9;
    goto LAB64;

LAB65:    xsi_size_not_matching(4U, t25, 0);
    goto LAB66;

LAB67:    xsi_set_current_line(71, ng0);
    t12 = (t0 + 3688U);
    t15 = *((char **)t12);
    t12 = (t0 + 5200);
    t19 = (t12 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t15, 4U);
    xsi_driver_first_trans_fast_port(t12);
    xsi_set_current_line(72, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 9064U);
    t5 = (t0 + 2728U);
    t8 = *((char **)t5);
    t5 = (t0 + 9096U);
    t12 = ieee_p_3620187407_sub_767740470_3965413181(IEEE_P_3620187407, t18, t4, t2, t8, t5);
    t15 = (t18 + 12U);
    t24 = *((unsigned int *)t15);
    t25 = (1U * t24);
    t1 = (4U != t25);
    if (t1 == 1)
        goto LAB70;

LAB71:    t19 = (t0 + 5264);
    t20 = (t19 + 56U);
    t21 = *((char **)t20);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    memcpy(t23, t12, 4U);
    xsi_driver_first_trans_fast_port(t19);
    xsi_set_current_line(73, ng0);
    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t2 = (t0 + 9064U);
    t5 = (t0 + 2608U);
    t8 = *((char **)t5);
    t5 = (t0 + 9080U);
    t1 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t4, t2, t8, t5);
    if (t1 != 0)
        goto LAB72;

LAB74:
LAB73:    goto LAB68;

LAB70:    xsi_size_not_matching(4U, t25, 0);
    goto LAB71;

LAB72:    xsi_set_current_line(74, ng0);
    t12 = (t0 + 3688U);
    t15 = *((char **)t12);
    t12 = (t0 + 5264);
    t19 = (t12 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t15, 4U);
    xsi_driver_first_trans_fast_port(t12);
    goto LAB73;

}


extern void work_a_1547472382_3712885429_init()
{
	static char *pe[] = {(void *)work_a_1547472382_3712885429_p_0};
	xsi_register_didat("work_a_1547472382_3712885429", "isim/tb_emissor_isim_beh.exe.sim/work/a_1547472382_3712885429.didat");
	xsi_register_executes(pe);
}

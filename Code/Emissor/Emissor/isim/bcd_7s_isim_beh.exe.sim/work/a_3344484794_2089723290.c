/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Brito/Documents/Repositorio/MultiplicadorVHDL/Code/Emissor/Emissor/bcd_7s.vhd";
extern char *WORK_P_4182573121;



static void work_a_3344484794_2089723290_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;

LAB0:    xsi_set_current_line(20, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB3;

LAB4:
LAB5:    t9 = (t0 + 3176);
    t10 = (t9 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = (unsigned char)2;
    xsi_driver_first_trans_delta(t9, 0U, 1, 0LL);

LAB2:    t14 = (t0 + 3080);
    *((int *)t14) = 1;

LAB1:    return;
LAB3:    t1 = (t0 + 3176);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_delta(t1, 0U, 1, 0LL);
    goto LAB2;

LAB6:    goto LAB2;

}

static void work_a_3344484794_2089723290_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    char *t5;
    int t6;
    char *t7;
    int t8;
    char *t9;
    int t10;
    char *t11;
    int t12;
    char *t13;
    int t14;
    char *t15;
    int t16;
    char *t17;
    int t18;
    char *t19;
    int t20;
    char *t21;
    int t22;
    char *t23;
    int t24;
    char *t25;
    int t26;
    char *t27;
    int t28;
    char *t29;
    int t30;
    char *t31;
    int t32;
    char *t33;
    int t34;
    char *t35;
    char *t36;
    char *t37;
    char *t38;
    char *t39;

LAB0:    xsi_set_current_line(26, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t1 = ((WORK_P_4182573121) + 3208U);
    t3 = *((char **)t1);
    t4 = xsi_mem_cmp(t3, t2, 4U);
    if (t4 == 1)
        goto LAB3;

LAB20:    t1 = ((WORK_P_4182573121) + 3328U);
    t5 = *((char **)t1);
    t6 = xsi_mem_cmp(t5, t2, 4U);
    if (t6 == 1)
        goto LAB4;

LAB21:    t1 = ((WORK_P_4182573121) + 3448U);
    t7 = *((char **)t1);
    t8 = xsi_mem_cmp(t7, t2, 4U);
    if (t8 == 1)
        goto LAB5;

LAB22:    t1 = ((WORK_P_4182573121) + 3568U);
    t9 = *((char **)t1);
    t10 = xsi_mem_cmp(t9, t2, 4U);
    if (t10 == 1)
        goto LAB6;

LAB23:    t1 = ((WORK_P_4182573121) + 3688U);
    t11 = *((char **)t1);
    t12 = xsi_mem_cmp(t11, t2, 4U);
    if (t12 == 1)
        goto LAB7;

LAB24:    t1 = ((WORK_P_4182573121) + 3808U);
    t13 = *((char **)t1);
    t14 = xsi_mem_cmp(t13, t2, 4U);
    if (t14 == 1)
        goto LAB8;

LAB25:    t1 = ((WORK_P_4182573121) + 3928U);
    t15 = *((char **)t1);
    t16 = xsi_mem_cmp(t15, t2, 4U);
    if (t16 == 1)
        goto LAB9;

LAB26:    t1 = ((WORK_P_4182573121) + 4048U);
    t17 = *((char **)t1);
    t18 = xsi_mem_cmp(t17, t2, 4U);
    if (t18 == 1)
        goto LAB10;

LAB27:    t1 = ((WORK_P_4182573121) + 4168U);
    t19 = *((char **)t1);
    t20 = xsi_mem_cmp(t19, t2, 4U);
    if (t20 == 1)
        goto LAB11;

LAB28:    t1 = ((WORK_P_4182573121) + 4288U);
    t21 = *((char **)t1);
    t22 = xsi_mem_cmp(t21, t2, 4U);
    if (t22 == 1)
        goto LAB12;

LAB29:    t1 = ((WORK_P_4182573121) + 4408U);
    t23 = *((char **)t1);
    t24 = xsi_mem_cmp(t23, t2, 4U);
    if (t24 == 1)
        goto LAB13;

LAB30:    t1 = ((WORK_P_4182573121) + 4528U);
    t25 = *((char **)t1);
    t26 = xsi_mem_cmp(t25, t2, 4U);
    if (t26 == 1)
        goto LAB14;

LAB31:    t1 = ((WORK_P_4182573121) + 4648U);
    t27 = *((char **)t1);
    t28 = xsi_mem_cmp(t27, t2, 4U);
    if (t28 == 1)
        goto LAB15;

LAB32:    t1 = ((WORK_P_4182573121) + 4768U);
    t29 = *((char **)t1);
    t30 = xsi_mem_cmp(t29, t2, 4U);
    if (t30 == 1)
        goto LAB16;

LAB33:    t1 = ((WORK_P_4182573121) + 4888U);
    t31 = *((char **)t1);
    t32 = xsi_mem_cmp(t31, t2, 4U);
    if (t32 == 1)
        goto LAB17;

LAB34:    t1 = ((WORK_P_4182573121) + 5008U);
    t33 = *((char **)t1);
    t34 = xsi_mem_cmp(t33, t2, 4U);
    if (t34 == 1)
        goto LAB18;

LAB35:
LAB19:    xsi_set_current_line(43, ng0);
    t1 = ((WORK_P_4182573121) + 2968U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);

LAB2:    t1 = (t0 + 3096);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(27, ng0);
    t1 = ((WORK_P_4182573121) + 1168U);
    t35 = *((char **)t1);
    t1 = (t0 + 3240);
    t36 = (t1 + 56U);
    t37 = *((char **)t36);
    t38 = (t37 + 56U);
    t39 = *((char **)t38);
    memcpy(t39, t35, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB4:    xsi_set_current_line(28, ng0);
    t1 = ((WORK_P_4182573121) + 1288U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB5:    xsi_set_current_line(29, ng0);
    t1 = ((WORK_P_4182573121) + 1408U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB6:    xsi_set_current_line(30, ng0);
    t1 = ((WORK_P_4182573121) + 1528U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB7:    xsi_set_current_line(31, ng0);
    t1 = ((WORK_P_4182573121) + 1648U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB8:    xsi_set_current_line(32, ng0);
    t1 = ((WORK_P_4182573121) + 1768U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB9:    xsi_set_current_line(33, ng0);
    t1 = ((WORK_P_4182573121) + 1888U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB10:    xsi_set_current_line(34, ng0);
    t1 = ((WORK_P_4182573121) + 2008U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB11:    xsi_set_current_line(35, ng0);
    t1 = ((WORK_P_4182573121) + 2128U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB12:    xsi_set_current_line(36, ng0);
    t1 = ((WORK_P_4182573121) + 2248U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB13:    xsi_set_current_line(37, ng0);
    t1 = ((WORK_P_4182573121) + 2368U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB14:    xsi_set_current_line(38, ng0);
    t1 = ((WORK_P_4182573121) + 2488U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB15:    xsi_set_current_line(39, ng0);
    t1 = ((WORK_P_4182573121) + 2608U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB16:    xsi_set_current_line(40, ng0);
    t1 = ((WORK_P_4182573121) + 2728U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB17:    xsi_set_current_line(41, ng0);
    t1 = ((WORK_P_4182573121) + 2848U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB18:    xsi_set_current_line(42, ng0);
    t1 = ((WORK_P_4182573121) + 2968U);
    t2 = *((char **)t1);
    t1 = (t0 + 3240);
    t3 = (t1 + 56U);
    t5 = *((char **)t3);
    t7 = (t5 + 56U);
    t9 = *((char **)t7);
    memcpy(t9, t2, 7U);
    xsi_driver_first_trans_delta(t1, 1U, 7U, 0LL);
    goto LAB2;

LAB36:;
}


extern void work_a_3344484794_2089723290_init()
{
	static char *pe[] = {(void *)work_a_3344484794_2089723290_p_0,(void *)work_a_3344484794_2089723290_p_1};
	xsi_register_didat("work_a_3344484794_2089723290", "isim/bcd_7s_isim_beh.exe.sim/work/a_3344484794_2089723290.didat");
	xsi_register_executes(pe);
}

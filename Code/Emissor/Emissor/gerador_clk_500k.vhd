library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity gerador_clk_500k is
    Port ( clk : in  STD_LOGIC;      -- entrada do clock a 50Mhz
			  clk_500k : out STD_LOGIC  -- saida do clock a aproximadamente 500khz 
		);
end gerador_clk_500k;

architecture gerador_clk_500k of gerador_clk_500k is

	signal contador: std_logic_vector(6 downto 0) := "0000000";  -- Sinal responsavel por contar o clk e retarda-lo para 500khz 
	signal clk_500k_i: STD_LOGIC :='0';
begin


	-- Processo responsavel por dividir o clock para 1khz permitindo a visualizaçao das informações no display de 7 segmentos --
	gerador: process(clk)
	begin
		if clk'event and clk = '1' then			
			if contador = "1100100" then -- 100
				clk_500k_i <= not clk_500k_i;
				contador <= "0000000";
			else
				contador <= contador + "1";
			end if;
		end if;
	end process gerador;
	clk_500k <= clk_500k_i;
end gerador_clk_500k;



LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY tb_emissor IS
END tb_emissor;
 
ARCHITECTURE behavior OF tb_emissor IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Emissor
    PORT(
         clk : IN  std_logic;
         botao_incremento : IN  std_logic;
         botao_decremento : IN  std_logic;
         switch_operando1 : IN  std_logic;
         switch_operando2 : IN  std_logic;
         display7seg : OUT  std_logic_vector(7 downto 0);
         enableDisplay : OUT  std_logic_vector(3 downto 0);
         botao_enviar : IN  std_logic;
         swd_p : OUT  std_logic;
         swd_n : OUT  std_logic;
         clk_p : OUT  std_logic;
         clk_n : OUT  std_logic
        );
    END COMPONENT;
    
   --Inputs
   signal clk : std_logic := '0';
   signal botao_incremento : std_logic := '0';
   signal botao_decremento : std_logic := '0';
   signal switch_operando1 : std_logic := '0';
   signal switch_operando2 : std_logic := '0';
   signal botao_enviar : std_logic := '0';

 	--Outputs
   signal display7seg : std_logic_vector(7 downto 0);
   signal enableDisplay : std_logic_vector(3 downto 0);
   signal swd_p : std_logic;
   signal swd_n : std_logic;
   signal clk_p : std_logic;
   signal clk_n : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Emissor PORT MAP (
          clk => clk,
          botao_incremento => botao_incremento,
          botao_decremento => botao_decremento,
          switch_operando1 => switch_operando1,
          switch_operando2 => switch_operando2,
          display7seg => display7seg,
          enableDisplay => enableDisplay,
          botao_enviar => botao_enviar,
          swd_p => swd_p,
          swd_n => swd_n,
          clk_p => clk_p,
          clk_n => clk_n
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;               -- seleciona o primeiro operando
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';
		wait for clk_period*10; -- incrementa 1 ao primeiro operando vai a 1
		botao_incremento <= '1';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';		
		wait for clk_period*10; -- incrementa 1 ao primeiro operando vai a 1
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';
		wait for clk_period*10; -- incrementa 1 ao primeiro operando vai a 2
		botao_incremento <= '1';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';		
		wait for clk_period*10; -- incrementa 1 ao primeiro operando vai a 2
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';
		wait for clk_period*10; -- incrementa 1 ao primeiro operando vai a 3
		botao_incremento <= '1';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';		
		wait for clk_period*10; -- incrementa 1 ao primeiro operando vai a 3
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';
		wait for clk_period*10; -- seleciona o segundo operando
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_enviar     <= '0';
		wait for clk_period*10; -- decrementa 1 ao segunda operando vai a 99
		botao_incremento <= '0';
		botao_decremento <= '1';
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_enviar     <= '0';
		wait for clk_period*10; -- decrementa 1 ao segunda operando vai a 99
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_enviar     <= '0';
		wait for clk_period*10; -- decrementa 1 ao segunda operando vai a 98
		botao_incremento <= '0';
		botao_decremento <= '1';
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_enviar     <= '0';
		wait for clk_period*10; -- decrementa 1 ao segunda operando vai a 98
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_enviar     <= '0';
		wait for clk_period*10; -- pressiona o bot�o enviar
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '0';
		switch_operando2 <= '0';
		botao_enviar     <= '1';
      wait;
   end process;

END;

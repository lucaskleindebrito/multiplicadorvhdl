library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity fsm_envio is
    Port ( clk_500k      : in STD_LOGIC; 
			  btn_enviar    : in STD_LOGIC := '0';	
			  valorDisplay0 : in std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 0
			  valorDisplay1 : in std_logic_vector(3 downto 0) := "0000";	 -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 1
		     valorDisplay2 : in std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 2
	        valorDisplay3 : in std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 3
			  bitSaida      : out STD_LOGIC := '1'
	 );
end fsm_envio;

architecture fsm_envio of fsm_envio is
	type maquina_estados is (esperar, carregar, enviar);
	signal estado 	   : maquina_estados;	
	signal pacote     : std_logic_vector(17 downto 0) := "000000000000000000";
	constant start    : std_logic := '0';
	constant finish   : std_logic := '1';
	constant UM       : std_logic_vector(4 downto 0) := "00001";
	signal contador   : std_logic_vector(4 downto 0) := "10001";	
begin
	
	fsm_process:process (clk_500k)
		begin							
			if clk_500k'event and clk_500k='1' then
				case estado is			
					when esperar => 					
						if btn_enviar = '0' then
							estado <= esperar;
						else
							estado <= carregar;
						end if;						
					when carregar => 				
						pacote(17) <= start; 
						pacote(16 downto 13) <= valorDisplay3; 
						pacote(12 downto 9)  <= valorDisplay2; 
						pacote(8 downto 5)   <= valorDisplay1; 
						pacote(4 downto 1)   <= valorDisplay0; 
						pacote(0) <= finish; 
						estado <= enviar;						
					when enviar => 	
						if contador = "10001" then
							bitSaida <= pacote(17);
						elsif contador = "10000" then
							bitSaida <= pacote(16);						
						elsif contador = "01111" then
							bitSaida <= pacote(15);
						elsif contador = "01110" then 
							bitSaida <= pacote(14);
						elsif contador = "01101" then 
							bitSaida <= pacote(13);
						elsif contador = "01100" then 
							bitSaida <= pacote(12);
						elsif contador = "01011" then 
							bitSaida <= pacote(11);
						elsif contador = "01010" then 
							bitSaida <= pacote(10);
						elsif contador = "01001" then 
							bitSaida <= pacote(9);
						elsif contador = "01000" then 
							bitSaida <= pacote(8);
						elsif contador = "00111" then 
							bitSaida <= pacote(7);
						elsif contador = "00110" then 
							bitSaida <= pacote(6);
						elsif contador = "00101" then 
							bitSaida <= pacote(5);
						elsif contador = "00100" then 
							bitSaida <= pacote(4);
						elsif contador = "00011" then 
							bitSaida <= pacote(3);
						elsif contador = "00010" then 
							bitSaida <= pacote(2);
						elsif contador = "00001" then 
							bitSaida <= pacote(1);
						elsif contador = "00000" then 
							contador <= "10001";
							bitSaida <= pacote(0);							
							estado <= esperar;
						end if;
				end case;				
				if estado = enviar then
					contador <= contador - UM;	
				end if;			
			end if;
		end process fsm_process;

end fsm_envio;


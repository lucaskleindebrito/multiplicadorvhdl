LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
  
ENTITY TestBenchEmissor IS
END TestBenchEmissor;
 
ARCHITECTURE behavior OF TestBenchEmissor IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT Emissor
    PORT(
         clk : IN  std_logic;
         botao_incremento : IN  std_logic;
         botao_decremento : IN  std_logic;
         switch_operando1 : IN  std_logic;
         switch_operando2 : IN  std_logic;
         display7seg : OUT  std_logic_vector(7 downto 0);
         enableDisplay : OUT  std_logic_vector(3 downto 0);
         enviar : IN  std_logic;
         swd_p : OUT  std_logic;
         swd_n : OUT  std_logic;
         clk_p : OUT  std_logic;
         clk_n : OUT  std_logic
        );
    END COMPONENT;
 
   --Inputs
   signal clk : std_logic := '0';
   signal botao_incremento : std_logic := '0';
   signal botao_decremento : std_logic := '0';
   signal switch_operando1 : std_logic := '0';
   signal switch_operando2 : std_logic := '0';
   signal enviar : std_logic := '0';

 	--Outputs
   signal display7seg : std_logic_vector(7 downto 0);
   signal enableDisplay : std_logic_vector(3 downto 0);
   signal swd_p : std_logic;
   signal swd_n : std_logic;
   signal clk_p : std_logic;
   signal clk_n : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Emissor PORT MAP (
          clk => clk,
          botao_incremento => botao_incremento,
          botao_decremento => botao_decremento,
          switch_operando1 => switch_operando1,
          switch_operando2 => switch_operando2,
          display7seg => display7seg,
          enableDisplay => enableDisplay,
          enviar => enviar,
          swd_p => swd_p,
          swd_n => swd_n,
          clk_p => clk_p,
          clk_n => clk_n
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
      wait for 100 ns;	
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_incremento <= '1';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_incremento <= '0';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_incremento <= '1';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_incremento <= '0';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_incremento <= '1';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_incremento <= '0';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_incremento <= '1';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_incremento <= '0';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_incremento <= '1';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_incremento <= '0';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_incremento <= '0';
		botao_decremento <= '1';
		wait for clk_period*10;
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_incremento <= '0';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_incremento <= '0';
		botao_decremento <= '1';
		wait for clk_period*10;
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_incremento <= '0';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_incremento <= '0';
		botao_decremento <= '1';
		wait for clk_period*10;
		switch_operando1 <= '0';
		switch_operando2 <= '0';
		botao_incremento <= '0';
		botao_decremento <= '0';
		wait for clk_period*10;
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_incremento <= '0';
		botao_decremento <= '0';
		enviar <= '1';		
		wait for clk_period*10;
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_incremento <= '0';
		botao_decremento <= '0';
		enviar <= '0';
		wait;


   end process;


		      

END;

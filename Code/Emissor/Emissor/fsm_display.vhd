library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fsm_display is
    Port ( clk_1k : in  STD_LOGIC;                                       -- Clock a 1khz
	 	     valorDisplay0 : in std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 0
			  valorDisplay1 : in std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 1
	        valorDisplay2 : in std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 2
	        valorDisplay3 : in std_logic_vector(3 downto 0) := "0000";    -- Sinal responsavel por armazenar o conte�do a ser visualizado no display 3
			  enableDisplay : out  STD_LOGIC_VECTOR (3 downto 0) := "1111"; -- S�ida para habilitar o display de 7 segmentos que deve ficar visivel
			  valor : out std_logic_vector(3 downto 0) := "0000"
			 );
end fsm_display;

architecture fsm_display of fsm_display is

	-- Constantes --
	constant DISPLAY1 : std_logic_vector(2 downto 0) := "000";
	constant DISPLAY2 : std_logic_vector(2 downto 0) := "001";
	constant DISPLAY3 : std_logic_vector(2 downto 0) := "010";
	constant DISPLAY4 : std_logic_vector(2 downto 0) := "011";
	
	-- sinais de controle --
	signal maq_est : std_logic_vector(2 downto 0);
begin

	-- Processo responsavel por apresentar as informa��es nos displays de 7 seg --
	fsm_process: process(clk_1k)
	begin
		if clk_1k'event and clk_1k = '1' then
			case maq_est is
				when DISPLAY1 =>
					enableDisplay <= "1110";	
					valor <= valorDisplay0;					
					maq_est <= DISPLAY2;
				when DISPLAY2 =>
					enableDisplay <= "1101";
					valor <= valorDisplay1;						
					maq_est <= DISPLAY3;
				when DISPLAY3 =>
					enableDisplay <= "1011";
					valor <= valorDisplay2;						
					maq_est <= DISPLAY4;
				when DISPLAY4 =>
					enableDisplay <= "0111";						
					valor <= valorDisplay3;	
					maq_est <= DISPLAY1;
				when others => -- � necessario colocar o when others quando utilizo constantes.
					maq_est <= DISPLAY1;		
			end case;	
		end if;	
	end process fsm_process;

end fsm_display;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity processa_operando is
    Port ( clk : in  STD_LOGIC;
			  btn_incrementa : in  STD_LOGIC   := '0';
			  switch_operando1 : in  STD_LOGIC := '0';
			  switch_operando2 : in  STD_LOGIC := '0';
			  btn_decrementa : in  STD_LOGIC   := '0';
			  valorDisplay0 : inout std_logic_vector(3 downto 0) := "0000";
			  valorDisplay1 : inout std_logic_vector(3 downto 0) := "0000";
	        valorDisplay2 : inout std_logic_vector(3 downto 0) := "0000";
	        valorDisplay3 : inout std_logic_vector(3 downto 0) := "0000"
	 );
end processa_operando;

architecture processa_operando of processa_operando is

	-- Constantes --	
	constant ZERO : std_logic_vector(3 downto 0)   := "0000";
	constant UM : std_logic_vector(3 downto 0) 	  := "0001";
	constant DOIS : std_logic_vector(3 downto 0)   := "0010";
	constant TRES : std_logic_vector(3 downto 0)   := "0011";
	constant QUATRO : std_logic_vector(3 downto 0) := "0100";
	constant CINCO : std_logic_vector(3 downto 0)  := "0101";
	constant SEIS : std_logic_vector(3 downto 0)   := "0110";
	constant SETE : std_logic_vector(3 downto 0)   := "0111";
	constant OITO : std_logic_vector(3 downto 0)   := "1000";
	constant NOVE : std_logic_vector(3 downto 0)   := "1001";

begin

-- Processo responsavel por incrementar e decrementar os operandos --
	processo: process(clk)
	begin
		if clk'event and clk = '1' then
			if btn_incrementa = '1' then
				if switch_operando1 = '1' and  switch_operando2 = '0' then
					valorDisplay0 <= valorDisplay0 + UM; -- incremento 1 ao operando 1
					if valorDisplay0 = NOVE then
						valorDisplay0 <= ZERO;
						valorDisplay1 <= valorDisplay1 + UM;
						if valorDisplay1 = NOVE then
							valorDisplay1 <= ZERO;
						end if;				
					end if;
				elsif switch_operando2 = '1' and  switch_operando1 = '0' then
					valorDisplay2 <= valorDisplay2 + UM; -- incremento 1 ao operando 2
					if valorDisplay2 = NOVE then
						valorDisplay2 <= ZERO;
						valorDisplay3 <= valorDisplay3 + UM;
						if valorDisplay3 = NOVE then
							valorDisplay3 <= ZERO;
						end if;				
					end if;
				end if;
			elsif btn_decrementa = '1' then
				if switch_operando1 = '1' and  switch_operando2 = '0' then
					valorDisplay0 <= valorDisplay0 - UM; -- decremento 1 ao operando 1
					if valorDisplay0 = ZERO then
						valorDisplay0 <= NOVE;
						valorDisplay1 <= valorDisplay1 - UM;
						if valorDisplay1 = ZERO then
							valorDisplay1 <= NOVE;
						end if;				
					end if;
				elsif switch_operando2 = '1' and  switch_operando1 = '0' then
					valorDisplay2 <= valorDisplay2 - UM; -- decremento 1 ao operando 2
					if valorDisplay2 = ZERO then
						valorDisplay2 <= NOVE;
						valorDisplay3 <= valorDisplay3 - UM;
						if valorDisplay3 = ZERO then
							valorDisplay3 <= NOVE;
						end if;				
					end if;
				end if;			
			end if;
		end if;	
	end process processo;


end processa_operando;


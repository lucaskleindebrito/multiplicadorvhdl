library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity fsm_bitabit is
	Port ( 
		clk          : in  STD_LOGIC;
		valorDisplay : in std_logic_vector(3 downto 0) := "0000";
		bitSaida     : out STD_LOGIC;
		avancar      : out STD_LOGIC
	 );
end fsm_bitabit;

architecture fsm_bitabit of fsm_bitabit is
	type maquina_estados is (rx_bit1, rx_bit2, rx_bit3, rx_bit4);
	signal estado 	 : maquina_estados;                 
begin

	fsm_process:process (clk)
	begin
		if clk'event and clk='1' then
			case estado is			
				when rx_bit1 => 
					avancar <= '0';
					bitSaida <= valorDisplay(0);
					estado <= rx_bit2;						
				when rx_bit2 => 
					bitSaida <= valorDisplay(1);
					estado <= rx_bit3;						
				when rx_bit3 => 
					bitSaida <= valorDisplay(2);
					estado <= rx_bit4;
				when rx_bit4 => 
					bitSaida <= valorDisplay(3);
					estado <= rx_bit1;
					avancar <= '1';
			end case;
		end if;
	end process fsm_process;
	
end fsm_bitabit;


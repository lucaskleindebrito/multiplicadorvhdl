library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity gerador_clk_1k is
    Port ( clk : in  STD_LOGIC;    -- entrada do clock a 50Mhz
			  clk_1k : out STD_LOGIC  -- saida do clock a aproximadamente 1khz
		);
end gerador_clk_1k;

architecture gerador_clk_1k of gerador_clk_1k is

	signal contador: std_logic_vector(15 downto 0) := "0000000000000000";  -- Sinal responsavel por contar o clk e retarda-lo para 1k 
	signal clk_1k_i: STD_LOGIC;
begin


	-- Processo responsavel por dividir o clock para 1khz permitindo a visualizaçao das informações no display de 7 segmentos --
	gerador: process(clk)
	begin
		if clk'event and clk = '1' then
			if contador = "1100001101010000" then
				clk_1k_i <= not clk_1k_i;
				contador <= "0000000000000000";
			else
				contador <= contador + "1";
			end if;
		end if;
	end process gerador;
	clk_1k <= clk_1k_i;
end gerador_clk_1k;


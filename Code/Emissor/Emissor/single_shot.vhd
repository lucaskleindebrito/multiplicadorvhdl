library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity single_shot is
    Port ( 
			clk : in  STD_LOGIC;
			sgn_in : in  STD_LOGIC;
			sgn_out : out  STD_LOGIC
		);
end single_shot;

architecture single_shot of single_shot is

	signal sr : std_logic_vector(2 downto 0);

begin

	sgn_out <= '1' when sr = "011" else '0';

	process(clk)
	begin
		if clk'event and clk = '1' then
			sr(0) <= sgn_in;
			sr(2 downto 1) <= sr(1 downto 0);
		end if;
	end process;

end single_shot;


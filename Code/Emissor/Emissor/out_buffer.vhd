library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity out_buffer is
	port (
			-- Portas de entrada de dados simples bit a bit
			bit1 : in std_logic  := '0';
			clk_o : in std_logic := '0';
			
			-- Portas de saida, para enviar as informações para o multiplicador
			swd_p : out std_logic  := '0';
			swd_n : out std_logic  := '0';
			clk_p  : out std_logic := '0';
			clk_n  : out std_logic := '0'			
		);
end out_buffer;

architecture out_buffer of out_buffer is
begin

   outbuf_inst_clk : OBUFDS   
   generic map (
      IOSTANDARD => "DEFAULT")
   port map (
		O  =>  clk_p, -- Diff_p output (connect directly to top-level port)
		OB =>  clk_n, -- Diff_n output (connect directly to top-level port)
		I  =>  clk_o  -- Buffer 
		
   );
	
   outbuf_inst_bit : OBUFDS    
   generic map (
      IOSTANDARD => "DEFAULT")
   port map (
		O  =>  swd_p, -- Diff_p output
		OB =>  swd_n, -- Diff_n output
		I  =>  bit1    -- Buffer 
	);
end out_buffer;


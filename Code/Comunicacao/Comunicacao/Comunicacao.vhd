library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Comunicacao is
	Port ( 
			 ---------------------- Portas Emissor ----------------------
			 clkEmissor       : in  STD_LOGIC;                      -- Clock do pino B8.
			 botao_incremento : in  STD_LOGIC;                      -- Bot�o de incremento.
          botao_decremento : in  STD_LOGIC;                      -- Bot�o de decremento.
			 switch_operando1 : in  STD_LOGIC;                      -- Switch para incrementar ou decrementar o operando 1.
			 switch_operando2 : in  STD_LOGIC;                      -- Switch para incrementar ou decrementar o operando 2.
			 display7seg      : out	std_logic_vector(7 downto 0);	  -- Sa�da para o Display de 7 segmentos.
          enableDisplay    : out  STD_LOGIC_VECTOR (3 downto 0); -- S�ida para habilitar o display de 7 segmentos que deve ficar visivel
			
			 -- Portas para o envio das informa��es 			 
			 botao_enviar : in STD_LOGIC;			 
			 ---------------------- Portas Emissor ----------------------
			 ---------------------- Portas Multiplicador ----------------------
			 		
			 clkMultiplicador	: in std_logic;
			 en_7s	         : out std_logic_vector(3 downto 0);
			 disp	            : out	std_logic_vector(7 downto 0)
			 
			 ---------------------- Portas Multiplicador ----------------------
			);	
end Comunicacao;

architecture Comunicacao of Comunicacao is
   signal swd_p  : std_logic;
	signal swd_n  : std_logic;			 
	signal clk_p  : std_logic;
	signal clk_n  : std_logic;

	signal clkd_p : std_logic;  
	signal clkd_n : std_logic;  
	signal bit0_p : std_logic;  
	signal bit0_n : std_logic;

	signal display0_emissor : std_logic_vector(3 downto 0);    
	signal display1_emissor : std_logic_vector(3 downto 0);	   
	signal display2_emissor : std_logic_vector(3 downto 0);    
	signal display3_emissor : std_logic_vector(3 downto 0);	
	signal display0_multiplicacao : std_logic_vector(3 downto 0);    
	signal display1_multiplicacao : std_logic_vector(3 downto 0);	   
	signal display2_multiplicacao : std_logic_vector(3 downto 0);    
	signal display3_multiplicacao : std_logic_vector(3 downto 0);	
	signal bitSaida_emissor : std_logic;
	signal clk_500k_emissor : std_logic;
	
	signal clk_in_multi : std_logic;
	signal sinal_input_multi : std_logic;
	signal clk_enable : std_logic;
	signal contador3 : std_logic_vector(3 downto 0);  
	
begin


	-- Inst�ncia do m�dulo "Emissor"
	emissor: entity work.Emissor
		port map( 
			clk => clkEmissor,
			botao_incremento => botao_incremento,
			botao_decremento => botao_decremento,
			switch_operando1 => switch_operando1,
			switch_operando2 => switch_operando2,
			display7seg      => display7seg,
			enableDisplay    => enableDisplay,
			botao_enviar     => botao_enviar,
			swd_p   			  => swd_p,
			swd_n            => swd_n,
			clk_p            => clk_p,
			clk_n            => clk_n,
			valorDisplay0_out => display0_emissor,    
			valorDisplay1_out => display1_emissor,	   
			valorDisplay2_out => display2_emissor,    
			valorDisplay3_out => display3_emissor,
			bitSaida_out => bitSaida_emissor,
			clk_500k_out => clk_500k_emissor
		);	
	
	-- Inst�ncia do m�dulo "Multiplicador"
	multiplicador: entity work.Multiplicador
		port map( 
			 clk	=> clkMultiplicador,
			 en_7s => en_7s,
			 disp	=> disp,
			 clkd_p => clkd_p,
			 clkd_n => clkd_n,
			 bit0_p => bit0_p,
			 bit0_n => bit0_n,
			 dado0_out => display0_multiplicacao,
			 dado1_out => display1_multiplicacao,
			 dado2_out => display2_multiplicacao,
			 dado3_out => display3_multiplicacao,
			 clk_in_multi => clk_in_multi,
			 sinal_input_multi => sinal_input_multi,
			 clk_enable => clk_enable,
			 contador3 => contador3
		);	
		
		clkd_p <= clk_p;
		clkd_n <= clk_n;
		bit0_p <= swd_p;
		bit0_n <= swd_n;


end Comunicacao;


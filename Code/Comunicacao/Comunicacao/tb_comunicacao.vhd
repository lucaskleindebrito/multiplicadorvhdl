LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY tb_comunicacao IS
END tb_comunicacao;
 
ARCHITECTURE behavior OF tb_comunicacao IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Comunicacao
    PORT(
         clkEmissor : IN  std_logic;
         botao_incremento : IN  std_logic;
         botao_decremento : IN  std_logic;
         switch_operando1 : IN  std_logic;
         switch_operando2 : IN  std_logic;
         display7seg : OUT  std_logic_vector(7 downto 0);
         enableDisplay : OUT  std_logic_vector(3 downto 0);
         botao_enviar : IN  std_logic;
         clkMultiplicador : IN  std_logic;
         en_7s : OUT  std_logic_vector(3 downto 0);
         disp : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clkEmissor : std_logic := '0';
   signal botao_incremento : std_logic := '0';
   signal botao_decremento : std_logic := '0';
   signal switch_operando1 : std_logic := '0';
   signal switch_operando2 : std_logic := '0';
   signal botao_enviar : std_logic := '0';
   signal clkMultiplicador : std_logic := '0';

 	--Outputs
   signal display7seg : std_logic_vector(7 downto 0);
   signal enableDisplay : std_logic_vector(3 downto 0);
   signal en_7s : std_logic_vector(3 downto 0);
   signal disp : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clkEmissor_period : time := 10 ns;
   constant clkMultiplicador_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Comunicacao PORT MAP (
          clkEmissor => clkEmissor,
          botao_incremento => botao_incremento,
          botao_decremento => botao_decremento,
          switch_operando1 => switch_operando1,
          switch_operando2 => switch_operando2,
          display7seg => display7seg,
          enableDisplay => enableDisplay,
          botao_enviar => botao_enviar,
          clkMultiplicador => clkMultiplicador,
          en_7s => en_7s,
          disp => disp
        );

  -- Clock process definitions
   clkEmissor_process :process
   begin				
		clkEmissor <= '0';
		wait for 1 ns;
		clkMultiplicador <= '0';
		wait for clkEmissor_period/2;
		clkEmissor <= '1';
		wait for 1 ns;
		clkMultiplicador <= '1';
		wait for clkEmissor_period/2;
		end process;
 
   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		wait for 100 ns;               -- seleciona o primeiro operando
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';
		wait for clkEmissor_period*10; -- incrementa 1 ao primeiro operando vai a 1
		botao_incremento <= '1';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';		
		wait for clkEmissor_period*10; -- incrementa 1 ao primeiro operando vai a 1
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';
		wait for clkEmissor_period*10; -- incrementa 1 ao primeiro operando vai a 2
		botao_incremento <= '1';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';		
		wait for clkEmissor_period*10; -- incrementa 1 ao primeiro operando vai a 2
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';
		wait for clkEmissor_period*10; -- incrementa 1 ao primeiro operando vai a 3
		botao_incremento <= '1';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';		
		wait for clkEmissor_period*10; -- incrementa 1 ao primeiro operando vai a 3
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '1';
		switch_operando2 <= '0';
		botao_enviar     <= '0';
		wait for clkEmissor_period*10; -- seleciona o segundo operando
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_enviar     <= '0';
		wait for clkEmissor_period*10; -- decrementa 1 ao segunda operando vai a 99
		botao_incremento <= '0';
		botao_decremento <= '1';
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_enviar     <= '0';
		wait for clkEmissor_period*10; -- decrementa 1 ao segunda operando vai a 99
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_enviar     <= '0';
		wait for clkEmissor_period*10; -- decrementa 1 ao segunda operando vai a 98
		botao_incremento <= '0';
		botao_decremento <= '1';
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_enviar     <= '0';
		wait for clkEmissor_period*10; -- decrementa 1 ao segunda operando vai a 98
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '0';
		switch_operando2 <= '1';
		botao_enviar     <= '0';
		wait for clkEmissor_period*10; -- pressiona o bot�o enviar
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '0';
		switch_operando2 <= '0';
		botao_enviar     <= '1';
		wait for clkEmissor_period*1500; -- pressiona o bot�o enviar
		botao_incremento <= '0';
		botao_decremento <= '0';
		switch_operando1 <= '0';
		switch_operando2 <= '0';
		botao_enviar     <= '0';      
		wait;
   end process;

END;

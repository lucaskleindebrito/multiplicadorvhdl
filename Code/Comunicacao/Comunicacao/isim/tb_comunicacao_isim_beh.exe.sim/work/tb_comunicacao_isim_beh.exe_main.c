/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *UNISIM_P_0947159679;
char *IEEE_P_1242562249;
char *IEEE_P_3620187407;
char *IEEE_P_3499444699;
char *WORK_P_4182573121;
char *IEEE_P_2592010699;
char *STD_STANDARD;
char *IEEE_P_2717149903;
char *IEEE_P_1367372525;
char *STD_TEXTIO;
char *UNISIM_P_3222816464;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    ieee_p_2592010699_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    ieee_p_1242562249_init();
    work_p_4182573121_init();
    unisim_p_0947159679_init();
    work_a_1697032314_0062117981_init();
    work_a_1681297051_2089723290_init();
    work_a_0234188038_1293609774_init();
    work_a_4067359479_0282554326_init();
    work_a_1092673351_0917098464_init();
    work_a_1547472382_3712885429_init();
    unisim_a_1478392591_3979135294_init();
    work_a_3579226689_4041547467_init();
    work_a_3843288248_0098144201_init();
    work_a_2951346850_1328736887_init();
    work_a_3067852167_2466357832_init();
    work_a_4141784843_2949573808_init();
    std_textio_init();
    ieee_p_2717149903_init();
    ieee_p_1367372525_init();
    unisim_p_3222816464_init();
    unisim_a_3428608661_4084842795_init();
    work_a_1875253068_4277260525_init();
    unisim_a_4160231357_3824467259_init();
    unisim_a_0995151376_0725860537_init();
    work_a_2694821013_2201088577_init();
    work_a_3930211969_2696398578_init();
    work_a_2764649485_1723026100_init();
    work_a_2800419804_3244783104_init();
    work_a_4200477426_2372691052_init();


    xsi_register_tops("work_a_4200477426_2372691052");

    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");
    WORK_P_4182573121 = xsi_get_engine_memory("work_p_4182573121");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_2717149903 = xsi_get_engine_memory("ieee_p_2717149903");
    IEEE_P_1367372525 = xsi_get_engine_memory("ieee_p_1367372525");
    STD_TEXTIO = xsi_get_engine_memory("std_textio");
    UNISIM_P_3222816464 = xsi_get_engine_memory("unisim_p_3222816464");

    return xsi_run_simulation(argc, argv);

}
